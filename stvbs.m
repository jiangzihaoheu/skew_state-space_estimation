function [xs, Ps] = stvbs(x0, P0, y, A, Q, C, R, Delta, Nu, Nvb)
% [xs, Ps] = stvbs(x0, P0, y, A, Q, C, R, Delta, Nu, Nvb)
%
% Computes the Skew t variational bayes smoother. The input is one
% measurement set of K time instants. The smoother is a fixed-interval
% smoother.
%
% Input:
%  x - prior mean of the initial state
%  P - prior covariance matrix of the initial state
%  y - 1xK cell array of measurement vectors
%  A - state transition matrix
%  Q - process noise covariance matrix
%  C - 1xK cell array of measurement model matrices
%  R - 1xK cell array of measurement noise spread parameters
%  Delta - 1xK cell array of diagonal matrices of skewness parameters
%  Nu - 1xK cell array of vectors of degrees of freedom
%  Nvb - number of VB iterations (30 by default)
% Output:
%  xs - n_x x K+1 matrix of the means of the smoother posterior
%  Ps - n_x x n_x x K+1 matrix of the covariances of the smoother posterior
%
% Henri Nurminen 1.3.2015

if nargin<10 || isempty(Nvb)
    Nvb = 30;
end

% Number of time instants
nm = numel(y);

bias = cell(1,nm); % variable for Delta*E_q[u]
ilambdamean = cell(1,nm); % variable for the vector of 1./E[\Lambda_ii]
for i = 1:nm
    % Initializing the VB iteration
    bias{i} = zeros(size(y{i}));
    ilambdamean{i} = ones(size(y{i}));
end

% Variational Bayes iterations
for n = 1:Nvb
    % State variable x
    [xs, Ps] = rtss(x0, P0, y, A, Q, C, R, bias, ilambdamean);
    
    for i = 1:nm
        iL = diag(ilambdamean{i});
        
        % Skewness variable u
        utilde = y{i}-C{i}*xs(:,i+1);
        Su = Delta{i}*Delta{i}' + R{i};
        Ku = Delta{i}'/Su;
        u = Ku * utilde;
        U = iL * (ones(numel(y{i}),1)-diag(Ku*Delta{i}));
        % Mean and variance of the half-normal distribution:
        sqrtdiagU = sqrt(U);
        trl = u ./ sqrtdiagU;
        tralpha = normcdf(trl);
        is_ok = tralpha>0;
        trepsilon = exp(-0.5*trl(is_ok).^2) ./ (sqrt(2*pi)*tralpha(is_ok));
        umean = u; %pre-allocation
        uvar = u;
        umean(is_ok) = u(is_ok) + sqrtdiagU(is_ok) .* trepsilon;
        uvar(is_ok) = U(is_ok) .* (1-trl(is_ok).*trepsilon-trepsilon.^2);
        % To avoid underflow
        umean(~is_ok) = 0;
        uvar(~is_ok) = 0;
        % The second monents of u
        Y = diag(uvar + umean.^2);
        
        % Kurtosis variable lambda
        Rinv = eye(size(R{i}))/R{i};
        D = Delta{i};
        Psi = Rinv*(utilde*utilde'+C{i}*Ps(:,:,i+1)*C{i}') ...
            + (D*Rinv*D+eye(size(D)))*Y ...
            - Rinv*D*umean*utilde' - D*Rinv*utilde*umean';
        alpha = Nu{i} + 2;
        beta = Nu{i} + diag(Psi);
        ilambdamean{i} = beta ./ alpha;
        
        % The expectation of the bias induced by the skewness model
        bias{i} = Delta{i}*umean;
    end
end
end


function [xs, Ps] = rtss(x0, P0, y, A, Q, C, R, ybias, ilambdamean)
% [xs, Ps] = rtss(x0, P0, y, A, Q, C, R, ybias, ilambdamean)
%
% Rauch-Tung-Striebel smoother
%
% Henri Nurminen 1.3.2015

nm = numel(y);
n_x = numel(x0);

% Filter predictions:
xmf = nan(n_x,nm);
Pmf = nan(n_x,n_x,nm);
% Filter posteriors:
xf = nan(n_x,nm+1);
Pf = nan(n_x,n_x,nm+1);
% Smoother posteriors:
xs = nan(n_x,nm+1);
Ps = nan(n_x,n_x,nm+1);

% Forward filtering
x = x0;
P = P0;
xf(:,1) = x0;
Pf(:,:,1) = P0;
for i = 1:nm
    xm = A*x;
    Pm = A*P*A' + Q;
    
    CC = C{i};
    S = CC*Pm*CC' + diag(ilambdamean{i}) * R{i};
    K = Pm*CC'/S;
    x = xm + K*(y{i}-ybias{i}-CC*xm);
    P = Pm - K*S*K';
    
    xmf(:,i) = xm;
    Pmf(:,:,i) = Pm;
    xf(:,i+1) = x;
    Pf(:,:,i+1) = P;
end

% Backward smoothing
xs(:,nm+1) = xf(:,nm+1);
Ps(:,:,nm+1) = Pf(:,:,nm+1);
for i = nm-1:-1:0
    G = Pf(:,:,i+1)*A'/Pmf(:,:,i+1);
    
    xs(:,i+1) = xf(:,i+1) + G*(xs(:,i+2)-xmf(:,i+1));
    Ps(:,:,i+1) = Pf(:,:,i+1) + G*(Ps(:,:,i+2)-Pmf(:,:,i+1))*G';
end
end