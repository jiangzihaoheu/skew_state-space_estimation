function [xs, Ps] = rtss(x0, P0, y, A, Q, C, R)
% [xs, Ps] = rtss(x0, P0, y, A, Q, C, R)
%
% Computes the Rauch-Tung-Striebel smoother. The input is one
% measurement set of K time instants. The smoother is a fixed-interval
% smoother.
%
% Input:
%  x - prior mean of the initial state
%  P - prior covariance matrix of the initial state
%  y - 1xK cell array of measurement vectors
%  A - state transition matrix
%  Q - process noise covariance matrix
%  C - 1xK cell array of measurement model matrices
%  R - 1xK cell array of measurement noise covariance matrices
% Output:
%  xs - n_x x K+1 matrix of the means of the smoother posterior
%  Ps - n_x x n_x x K+1 matrix of the covariances of the smoother posterior
%
% Henri Nurminen 1.3.2015

nm = numel(y);
n_x = numel(x0);

% Filter predictions:
xmf = nan(n_x,nm);
Pmf = nan(n_x,n_x,nm);
% Filter posteriors:
xf = nan(n_x,nm+1);
Pf = nan(n_x,n_x,nm+1);
% Smoother posteriors:
xs = nan(n_x,nm+1);
Ps = nan(n_x,n_x,nm+1);

% Forward filtering
x = x0;
P = P0;
xf(:,1) = x0;
Pf(:,:,1) = P0;
for i = 1:nm
    xm = A*x;
    Pm = A*P*A' + Q;
    
    CC = C{i};
    S = CC*Pm*CC' + R{i};
    
    % Measurement validation by gating
    outlier = ((y{i}-CC*xm).^2./diag(S))>chi2inv(0.99,1);
    y{i}(outlier) = [];
    CC(outlier,:) = [];
    S(outlier,:) = [];
    S(:,outlier) = [];
    
    K = Pm*CC'/S;
    x = xm + K*(y{i}-CC*xm);
    P = Pm - K*S*K';
    
    xmf(:,i) = xm;
    Pmf(:,:,i) = Pm;
    xf(:,i+1) = x;
    Pf(:,:,i+1) = P;
end

% Backward smoothing
xs(:,nm+1) = xf(:,nm+1);
Ps(:,:,nm+1) = Pf(:,:,nm+1);
for i = nm-1:-1:0
    G = Pf(:,:,i+1)*A'/Pmf(:,:,i+1);
    
    xs(:,i+1) = xf(:,i+1) + G*(xs(:,i+2)-xmf(:,i+1));
    Ps(:,:,i+1) = Pf(:,:,i+1) + G*(Ps(:,:,i+2)-Pmf(:,:,i+1))*G';
end
end