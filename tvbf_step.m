function [x, P] = tvbf_step(x, P, y, A, Q, C, R, Nu, Nvb)
% [x, P] = tvbf_step(x, P, y, A, Q, C, R, Nu, Nvb)
%
% Computes one measurement update of the t variational Bayes filter.
%
% Input:
%  x - prior mean of the state
%  P - prior covariance matrix of the state
%  y - measurement vector
%  A - state transition matrix
%  Q - process noise covariance matrix
%  C - measurement model matrix
%  R - measurement noise shape matrix
%  Nu - vector of degrees of freedom
%  Nvb - number of VB iterations (30 by default)
% Output:
%  x - the mean of the posterior
%  P - the covariance matrix of the posterior
%
% Henri Nurminen 1.3.2015

if nargin<9 || isempty(Nvb)
    Nvb = 30;
end

% Prediction step
xm = A*x;
Pm = A*P*A' + Q;

n_y = numel(y);
if n_y==0
    x = xm;
    P = Pm;
    return;
end

% Inverse of the shape matrix
Rinv = eye(size(R))/R;

% Intializing the kurtosis variable lambda
mlambda = ones(n_y,1);

% Variational Bayes iterations
for i = 1:Nvb
    Lambdainv = diag(1./mlambda);
    
    % State variable x
    Sx = C*Pm*C' + Lambdainv*R;
    Kx = Pm*C'/Sx;
    x = xm + Kx*(y-C*xm);
    P = Pm - Kx*Sx*Kx';
    
    % Kurtosis variable lambda
    dd = y-C*x;
    Psi = Rinv * (dd*dd' + C*P*C');
    alpha = Nu + 1;
    beta = Nu + diag(Psi);
    mlambda = alpha ./ beta;
end

P = 0.5*(P+P');