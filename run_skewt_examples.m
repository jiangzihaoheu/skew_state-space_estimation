% Usage example of the algorithms that are compared in the papers
% 
% H. Nurminen, T. Ardeshiri, R. Piche, F. Gustafsson. "Robust Inference for 
% State-Space Models with Skewed Measurement Noise". IEEE Signal Processing 
% Letters, vol. 22, no. 11, pp. 1898-1902, November 2015.
%
% and
%
% H. Nurminen, T. Ardeshiri, R. Piche, F. Gustafsson. "Skew-t inference 
% with improved covariance matrix approximation". 2015. [Online]. Available:
% http://arxiv.org/abs/1603.06216.
%
% Author: Henri Nurminen
%  v1.0: 19.5.2015
%  v2.0: 30.3.2016, Added STF and STS

%% Parameters
nm = 100;       % number of time instants

q = 5;         % process noise parameter
r = 1;          % measurement noise spread parameter
delta = 5;      % measurement noise skewness parameter
nu = 4;         % measurement noise degree-of-freedom parameter

NVB_stf = 5;    % # VB iterations for STF (filter)
NVB_stvbf = 30; % # VB iterations for STVBF (filter)
NVB_tvbf = 10;  % # VB iterations for TVBF (filter)
NVB_sts = 5;    % # VB iterations for STS (smoother)
NVB_stvbs = 30; % # VB iterations for STVBS (smoother)
NVB_tvbs = 10;  % # VB iterations for TVBS (smoother)


%% Determine model matrices
% Motion model:
dx = 4;
A = eye(dx);
Q = [diag([repmat(q^2,[1,dx-2]),0.5^2]), zeros(dx-1,1); zeros(1,dx-1), 0];
m0 = zeros(dx,1);
P0 = [diag([repmat(10^2,[1,dx-2]),10^2]), zeros(dx-1,1); zeros(1,dx-1), 0.75^2];

% Measurement model:
load('satconst', 'S');
if nm>numel(S)
    warning(['Only ' num2str(numel(S)) ' measurements used.']);
    nm = numel(S);
end
S = S(1:nm);
c = @(x,k) sqrt(sum(bsxfun(@minus,S{k},x(1:3)).^2,1))'+x(4);
C = @(x,k) [bsxfun(@rdivide, bsxfun(@minus,x(1:3),S{k}), ...
    sqrt(sum(bsxfun(@minus,S{k},x(1:3)).^2,1)))', ones(size(S{k},2),1)];
R = cell(1,nm);
Delta = cell(1,nm);
Nu = cell(1,nm);
for i = 1:nm
    ny = size(S{i},2);
    
    R{i} = r^2 * eye(ny);
    Delta{i} = diag(repmat(delta,[1,ny]));
    Nu{i} = repmat(nu,[ny,1]);
end

% Mean and variance for the measurement noise:
[meani, vari] = skewt_stats(0, r, delta, nu);


%% Generate the data

x = nan(dx,nm+1);
y = cell(1,nm);

w = warning('off','all');
sqrtmQ = sqrtm(Q);
warning(w);
x(:,1) = m0 + sqrtm(P0)*randn(dx,1);
for i = 1:nm
    w = sqrtmQ * randn(dx,1);
    x(:,i+1) = A*x(:,i) + w;
    y{i} = c(x(:,i+1),i);
    ny = numel(y{i});
    
    lambda = gamrnd(Nu{i}/2,2./Nu{i});
    u = abs(randn(ny,1));
    v = sqrtm(R{i})*randn(ny,1);
    y{i} = y{i} + (Delta{i}*u + v) ./ sqrt(lambda);
end


%% STF
mx_stf = nan(dx,nm+1);
Px_stf = nan(dx,dx,nm+1);

% For linearizations measurement model matrices C and measurements with the
% linearization's constants subtracted:
C_stf = cell(1,nm);
y_stf = cell(1,nm);

mx_stf(:,1) = m0;
Px_stf(:,:,1) = P0;
for i = 1:nm
    C_stf{i} = C(mx_stf(:,i),i);
    y_stf{i} = y{i} - c(mx_stf(:,i),i) + C_stf{i}*mx_stf(:,i);
    
    [mx_stf(:,i+1),Px_stf(:,:,i+1)] = stf_step(mx_stf(:,i), Px_stf(:,:,i), ...
        y_stf{i}, A, Q, C_stf{i}, R{i}, Delta{i}, Nu{i}, NVB_stf);
end
rmse_stf = sqrt(mean(sum((mx_stf(1:3,:)-x(1:3,:)).^2,1)));


%% STVBF
mx_stvbf = nan(dx,nm+1);
Px_stvbf = nan(dx,dx,nm+1);

% For linearizations measurement model matrices C and measurements with the
% linearization's constants subtracted:
C_stvbf = cell(1,nm);
y_stvbf = cell(1,nm);

mx_stvbf(:,1) = m0;
Px_stvbf(:,:,1) = P0;
for i = 1:nm
    C_stvbf{i} = C(mx_stvbf(:,i),i);
    y_stvbf{i} = y{i} - c(mx_stvbf(:,i),i) + C_stvbf{i}*mx_stvbf(:,i);
    
    [mx_stvbf(:,i+1),Px_stvbf(:,:,i+1)] = stvbf_step(mx_stvbf(:,i), Px_stvbf(:,:,i), ...
        y_stvbf{i}, A, Q, C_stvbf{i}, R{i}, Delta{i}, Nu{i}, NVB_stvbf);
end
rmse_stvbf = sqrt(mean(sum((mx_stvbf(1:3,:)-x(1:3,:)).^2,1)));


%% TVBF
mx_tvbf = nan(dx,nm+1);
Px_tvbf = nan(dx,dx,nm+1);

C_t = cell(1,nm);
y_t = cell(1,nm);
shape_mtx = cell(1,nm);

mx_tvbf(:,1) = m0;
Px_tvbf(:,:,1) = P0;
for i = 1:nm
    C_t{i} = C(mx_tvbf(:,i),i);
    % Skew-t's mean and (nu-2)/nu times covariance matrix taken into account
    y_t{i} = y{i} - c(mx_tvbf(:,i),i) + C_t{i}*mx_tvbf(:,i) - meani;
    shape_mtx{i} = (nu-2)/nu * vari * eye(numel(y_t{i}));
    
    [mx_tvbf(:,i+1),Px_tvbf(:,:,i+1)] = tvbf_step(mx_tvbf(:,i), Px_tvbf(:,:,i), ...
        y_t{i}, A, Q, C_t{i}, shape_mtx{i}, Nu{i}, NVB_tvbf);
end
rmse_tvbf = sqrt(mean(sum((mx_tvbf(1:3,:)-x(1:3,:)).^2,1)));


%% KF-G
mx_kf = nan(dx,nm+1);
Px_kf = nan(dx,dx,nm+1);

% For linearizations measurement model matrices and measurements with the
% linearization's constants subtracted:
C_k = cell(1,nm);
y_k = cell(1,nm);
cov_mtx = cell(1,nm);

mx_kf(:,1) = m0;
Px_kf(:,:,1) = P0;
for i = 1:nm
    C_k{i} = C(mx_kf(:,i),i);
    % Skew-t's mean and covariance matrix taken into account
    y_k{i} = y{i} - c(mx_kf(:,i),i) + C_k{i}*mx_kf(:,i) - meani;
    cov_mtx{i} = vari * eye(numel(y_k{i}));
    
    [mx_kf(:,i+1),Px_kf(:,:,i+1)] = kf_step(mx_stvbf(:,i), Px_stvbf(:,:,i), ...
        y_k{i}, A, Q, C_k{i}, cov_mtx{i});
end
rmse_kf = sqrt(mean(sum((mx_kf(1:3,:)-x(1:3,:)).^2,1)));

%% STS
% (Uses the same linearization points as the filter for the readibility of 
% the code. Linearization errors are negligible. In the papers' simulations
% the standard linearization was used.)
[mx_sts, Px_sts] = sts(m0, P0, y_stvbf, A, Q, C_stf, R, Delta, Nu, NVB_sts);
rmse_sts = sqrt(mean(sum((mx_sts(1:3,:)-x(1:3,:)).^2,1)));


%% STVBS
[mx_stvbs, Px_stvbs] = stvbs(m0, P0, y_stvbf, A, Q, C_stvbf, R, Delta, Nu, NVB_stvbs);
rmse_stvbs = sqrt(mean(sum((mx_stvbs(1:3,:)-x(1:3,:)).^2,1)));


%% TVBS
[mx_tvbs, Px_tvbs] = tvbs(m0, P0, y_t, A, Q, C_t, shape_mtx, Nu, NVB_tvbs);
rmse_tvbs = sqrt(mean(sum((mx_tvbs(1:3,:)-x(1:3,:)).^2,1)));


%% RTSS-G
[mx_rtss, Px_rtss] = rtss(m0, P0, y_k, A, Q, C_k, cov_mtx);
rmse_rtss = sqrt(mean(sum((mx_rtss(1:3,:)-x(1:3,:)).^2,1)));


%% Displaying the RMSE
disp('RMSE');
ff = '%0.2f';
disp(['Skew t filter: ' num2str(rmse_stf,ff)]);
disp(['Skew t VB filter: ' num2str(rmse_stvbf,ff)]);
disp(['t VB filter: ' num2str(rmse_tvbf,ff)]);
disp(['Kalman filter: ' num2str(rmse_kf,ff)]);
disp(['Skew t smoother: ' num2str(rmse_sts,ff)]);
disp(['Skew t VB smoother: ' num2str(rmse_stvbs,ff)]);
disp(['t VB smoother: ' num2str(rmse_tvbs,ff)]);
disp(['RTS smoother: ' num2str(rmse_rtss,ff)]);