function [meani, covi] = skewt_stats(Mu, R, Delta, Nu)
% Expectation and covariance matrix of skew t-distribution. Based on the
% formulas given by
%
% S. K. Sahu, D. K. Dey, and M. D. Branco, ?A new class of multivariate 
% skew distributions with applications to Bayesian regression models,? 
% Canadian Journal of Statistics, vol. 31, no. 2, pp. 129?150, 2003.
%
% and
%
% S. K. Sahu, D. K. Dey, and M. D. Branco, ?Erratum: A new class of 
% multivariate skew distributions with applications to Bayesian regression 
% models,? Canadian Journal of Statistics, vol. 37, no. 2, pp. 301?302, 
% 2009.
% 
% Input:
%  Mu - nx1 matrix, location parameter
%  R - nxn diagonal matrix, spread parameter matrix
%  Delta - nxn diagonal matrix, skewness parameter matrix
%  Nu - nx1 matrix, degrees of freedom parameters
% Output:
%  meani - nx1 matrix, expectation value
%  covi - nxn matrix, covariance matrix
%
% Henri Nurminen 1.3.2015
n_y = numel(Mu);

meani = nan(n_y,1);
covi = zeros(n_y);
for i = 1:n_y
    nui = Nu(i);
    Ri = R(i,i);
    Deltai = Delta(i,i);
    gammaf = sqrt(nui/pi) * gamma((nui-1)/2)/gamma(nui/2);
    meani(i) = gammaf * Deltai * ones(n_y,1);
    covi(i,i) = (nui/(nui-2)) * (Ri + (1-2/pi)*(Deltai*Deltai')) ...
        + (2/pi*nui/(nui-2)-gammaf^2) * (Deltai*ones(n_y)*Deltai');
end
meani = meani + Mu;