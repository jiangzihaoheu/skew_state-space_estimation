function [x, P] = kf_step(x, P, y, A, Q, C, R)
% [x, P] = kf_step(x, P, y, A, Q, C, R, Delta, Nu, Nvb)
%
% Computes one measurement update of the Kalman filter with gating.
%
% Input:
%  x - prior mean of the state
%  P - prior covariance matrix of the state
%  y - measurement vector
%  A - state transition matrix
%  Q - process noise covariance matrix
%  C - measurement model matrix
%  R - measurement noise covariance matrix
% Output:
%  x - the mean of the posterior
%  P - the covariance matrix of the posterior
%
% Henri Nurminen 1.3.2015

% Prediction step
xm = A*x;
Pm = A*P*A' + Q;

n_y = numel(y);
if n_y==0
    x = xm;
    P = Pm;
    return;
end

% State variable x
S = C*Pm*C' + R;

% Measurement validation by gating
outlier = ((y-C*xm).^2./diag(S))>chi2inv(0.99,1);
y(outlier) = [];
C(outlier,:) = [];
S(outlier,:) = [];
S(:,outlier) = [];

Kx = Pm*C'/S;
x = xm + Kx*(y-C*xm);
P = Pm - Kx*S*Kx';

P = 0.5*(P+P');